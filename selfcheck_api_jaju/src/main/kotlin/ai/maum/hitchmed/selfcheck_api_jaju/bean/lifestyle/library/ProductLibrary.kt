package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library

import ai.maum.hitchmed.selfcheck_api_jaju.model.factory.ProductRecordFactory
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.ProductRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.ProductRecordRaw
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Component
import java.io.File
import javax.annotation.PostConstruct

@Component
class ProductLibrary {
    var library: List<ProductRecord> = listOf()
    var beautyHashtags: List<String> = listOf()

    @PostConstruct
    fun postConstruct() {
        val data = Json { isLenient = true }.decodeFromString<List<ProductRecordRaw>>(File("./hitchmed_products.json").readText())

        library = data.map { ProductRecordFactory.of(it) }
        beautyHashtags = library.filter { it.category == "뷰티" }.map { it.hashtag_numbering }
    }
}