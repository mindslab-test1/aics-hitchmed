package ai.maum.hitchmed.selfcheck_api_jaju.filter

import ai.maum.hitchmed.selfcheck_api_jaju.filter.extension.FilterImpl
import ai.maum.hitchmed.selfcheck_api_jaju.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.http.HttpStatus
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class HelloFilterImpl(
        var userInfoFactory: ObjectFactory<UserInfo>,
) : FilterImpl {
    @Throws(IOException::class, ServletException::class)
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        try {
            userInfoFactory.`object`.tbd = when (request.getHeader("tbd")) {
                null -> null
                else -> throw Exception()
            }
        } catch (e: Exception) {
            response.status = HttpStatus.UNAUTHORIZED.value()
            return
        }

        chain.doFilter(request, response)
    }
}