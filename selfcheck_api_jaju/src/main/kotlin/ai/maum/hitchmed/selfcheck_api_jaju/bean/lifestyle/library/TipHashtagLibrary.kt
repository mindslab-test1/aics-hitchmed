package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.TipHashtagLibraryException
import ai.maum.hitchmed.selfcheck_api_jaju.model.factory.TipHashtagRecordFactory
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.TipHashtagRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.TipHashtagRecordRaw
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Component
import java.io.File
import javax.annotation.PostConstruct

@Component("tipHashtagLibrary")
class TipHashtagLibrary {
    var library: List<TipHashtagRecord> = listOf()

    @PostConstruct
    fun postConstruct() {
        val data = Json { isLenient = true }.decodeFromString<List<TipHashtagRecordRaw>>(File("./hitchmed_tip_hashtags.json").readText())

        library = data.map { TipHashtagRecordFactory.of(it) }
    }

    fun of(numbering: String): TipHashtagRecord = library.find { it.hashtag_numbering == numbering }
            ?: throw TipHashtagLibraryException(1, "Invalid numbering")
}