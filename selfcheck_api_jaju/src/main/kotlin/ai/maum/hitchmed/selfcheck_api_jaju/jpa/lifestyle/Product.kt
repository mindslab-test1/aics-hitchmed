package ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Product : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "PRODUCT_SEQ_GEN",
            sequenceName = "PRODUCT_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var uniqueId: String? = null

    @Column(nullable = false)
    var code: String? = null
}