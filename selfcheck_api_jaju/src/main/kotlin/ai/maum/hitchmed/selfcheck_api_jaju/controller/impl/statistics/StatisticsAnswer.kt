package ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.controller.StatisticsController
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.HandlerType
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.ResponseType
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsAnswerRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsAnswerResponseDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsAnswerResponseItemDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class StatisticsAnswer(controller: StatisticsController, dto: StatisticsAnswerRequestDto)
    : HandlerType<StatisticsController, StatisticsAnswerRequestDto>(controller, dto) {

    override fun invoke(): ResponseType {
        val date = dto.date
        val after = Instant.parse("${date.subSequence(0, 4)}-${date.subSequence(4, 6)}-${date.subSequence(6, 8)}T00:00:00.00Z").minusMillis(1)
        val before = after.plusMillis(1000 * 60 * 60 * 24 + 1)

        // 필요한 날짜는 UTC+9의 관점에서 0시~24시
        // 즉 UTC+0의 관점에서 -9시~15시를 가져오면 됨
        val utcAfter = after.minusMillis(1000 * 60 * 60 * 9)
        val utcBefore = before.minusMillis(1000 * 60 * 60 * 9)

        val answerStatistics = controller.answerStatisticsRepository.findByDateBeforeAndDateAfter(utcBefore, utcAfter)

        val result = mutableListOf<StatisticsAnswerResponseItemDto>()
        for (answer in controller.answerLibrary.library) {
            // 주관식 스킵
            if (answer.answer in listOf(101L, 301L)) continue
            val item = StatisticsAnswerResponseItemDto(
                    anwno = answer.answer,
                    count = 0
            )

            result.add(item)
        }

        for (cell in answerStatistics) {
            val item = result.find { it.anwno == cell.answer }
                    ?: continue // invalid answer
            item.count = cell.count!!
        }

        return ResponseEntity.ok(StatisticsAnswerResponseDto(result))
    }
}