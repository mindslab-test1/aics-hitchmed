package ai.maum.hitchmed.selfcheck_api_jaju.model.statistics

import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

enum class AgeGroup(private val value: String) {
    R0_24("0_24"),
    R25_34("25_34"),
    R35_44("35_44"),
    R45_54("45_54"),
    R55_64("55_64"),
    R65_99("65_99"),
    ERROR("ERROR");

    override fun toString(): String = value

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)

        fun fromAge(age: Long): AgeGroup = when (age) {
            in 0..24 -> R0_24
            in 25..34 -> R25_34
            in 35..44 -> R35_44
            in 45..54 -> R45_54
            in 55..64 -> R55_64
            in 65..99 -> R65_99
            else -> ERROR
        }

        fun fromBirth(yyyy: String): AgeGroup =
                try {
                    if (!Regex("^[12][0-9]{3}$").matches(yyyy))
                        throw RuntimeException()
                    else
                    // Korean age calc
                        fromAge(
                                LocalDateTime.now()
                                        .format(DateTimeFormatter.ISO_DATE)
                                        .substring(0..3)
                                        .toLong()
                                        - yyyy.toLong() + 1
                        )
                } catch (e: Exception) {
                    logger.warn("AgeGroup companion object fun `from` formatting error: yyyy=$yyyy")
                    ERROR
                }

        fun fromEntity(age: String): AgeGroup = when (age) {
            "0_24" -> R0_24
            "25_34" -> R25_34
            "35_44" -> R35_44
            "45_54" -> R45_54
            "55_64" -> R55_64
            "65_99" -> R65_99
            else -> ERROR
        }

        val validRange = listOf(R0_24, R25_34, R35_44, R45_54, R55_64, R65_99)
    }
}