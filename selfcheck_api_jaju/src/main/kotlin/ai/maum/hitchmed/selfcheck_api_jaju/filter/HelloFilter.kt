package ai.maum.hitchmed.selfcheck_api_jaju.filter

import ai.maum.hitchmed.selfcheck_api_jaju.filter.extension.FilterUrlWrapper
import ai.maum.hitchmed.selfcheck_api_jaju.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(0)
@Component
class HelloFilter(userInfoFactory: ObjectFactory<UserInfo>) : FilterUrlWrapper(HelloFilterImpl(userInfoFactory)) {
    init {
        skipRules {
            skip equals "/test1" equals "/test2"
            skip equals "/test3"

            skip startsWith "/admin/"
        }
    }
}