package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input

class UserShortAnswer(question: Long, var answer_short: String) : UserAnswer(true, question)