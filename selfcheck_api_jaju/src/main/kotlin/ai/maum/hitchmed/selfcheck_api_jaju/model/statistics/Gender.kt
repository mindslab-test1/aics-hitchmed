package ai.maum.hitchmed.selfcheck_api_jaju.model.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.GenderException

enum class Gender {
    MALE,
    FEMALE,
    NOT_CHOSEN;

    override fun toString(): String = when (this) {
        MALE -> "남"
        FEMALE -> "여"
        NOT_CHOSEN -> "선택안함"
    }

    companion object {
        fun fromEntity(gender: String) = when (gender) {
            "남" -> MALE
            "여" -> FEMALE
            "선택안함" -> NOT_CHOSEN
            else -> throw GenderException(1, "Cannot parse gender from entity")
        }

        val validRange = listOf(MALE, FEMALE, NOT_CHOSEN)
    }
}