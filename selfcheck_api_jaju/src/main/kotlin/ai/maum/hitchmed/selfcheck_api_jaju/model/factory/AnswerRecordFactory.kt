package ai.maum.hitchmed.selfcheck_api_jaju.model.factory

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.AnswerRecordFactoryException
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.WeightedHashtag
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecordRaw

class AnswerRecordFactory private constructor() {
    companion object {
        fun of(from: AnswerRecordRaw): AnswerRecord {
            val recommend_weight = if (from.recommend_weight.isNotEmpty()) from.recommend_weight.split(',').map { it.trim() }.map { toDouble(it) } else listOf()
            val recommend_hashtag_numbering = if (from.recommend_hashtag_numbering.isNotEmpty()) from.recommend_hashtag_numbering.split(',').map { it.trim() } else listOf()
            val lifestyle_weight = if (from.lifestyle_weight.isNotEmpty()) from.lifestyle_weight.split(',').map { it.trim() }.map { toDouble(it) } else listOf()
            val lifestyle_hashtag_numbering = if (from.lifestyle_hashtag_numbering.isNotEmpty()) from.lifestyle_hashtag_numbering.split(',').map { it.trim() } else listOf()

            if (recommend_weight.size != recommend_hashtag_numbering.size)
                throw AnswerRecordFactoryException(1, "Recommendation hashtag and weight list size mismatch ${recommend_weight.size}, ${recommend_hashtag_numbering.size}, ${from.answer}, ${from.recommend_hashtag_numbering}")

            if (lifestyle_weight.size != lifestyle_hashtag_numbering.size)
                throw AnswerRecordFactoryException(2, "Life style hashtag and weight list size mismatch ${lifestyle_weight.size}, ${lifestyle_hashtag_numbering.size}, ${from.answer}")

            val recommendations = mutableListOf<WeightedHashtag>()
            val lifestyles = mutableListOf<WeightedHashtag>()

            for (i in recommend_weight.indices) {
                recommendations.add(WeightedHashtag(recommend_weight[i], recommend_hashtag_numbering[i]))
            }

            for (i in lifestyle_weight.indices) {
                lifestyles.add(WeightedHashtag(lifestyle_weight[i], lifestyle_hashtag_numbering[i]))
            }

            return AnswerRecord(
                    answer = from.answer,
                    group_scoring_fitness_health = from.group_scoring_fitness_health,
                    group_scoring_fatigue_sleep = from.group_scoring_fatigue_sleep,
                    group_scoring_stress_immunity = from.group_scoring_stress_immunity,
                    group_scoring_mbti_energy_extroversion = from.group_scoring_mbti_energy_extroversion,
                    group_scoring_mbti_information_sensing = from.group_scoring_mbti_information_sensing,
                    group_scoring_mbti_decision_thinking = from.group_scoring_mbti_decision_thinking,
                    group_scoring_mbti_lifestyle_judging = from.group_scoring_mbti_lifestyle_judging,
                    recommendation = recommendations,
                    lifestyle = lifestyles
            )
        }

        fun toDouble(str: String): Double =
                if (str == "") .0
                else str.toDouble()
    }
}