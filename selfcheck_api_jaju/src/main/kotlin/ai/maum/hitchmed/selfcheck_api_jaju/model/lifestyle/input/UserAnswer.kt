package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input

abstract class UserAnswer protected constructor(
        var is_short: Boolean,
        var question: Long
)