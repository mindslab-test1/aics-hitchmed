package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import kotlinx.serialization.Serializable

@Serializable
data class AnswerRecordRaw(
        var answer: Long = 0,
        var group_scoring_fitness_health: Long = 0,
        var group_scoring_fatigue_sleep: Long = 0,
        var group_scoring_stress_immunity: Long = 0,
        var group_scoring_mbti_energy_extroversion: Long = 0,
        var group_scoring_mbti_information_sensing: Long = 0,
        var group_scoring_mbti_decision_thinking: Long = 0,
        var group_scoring_mbti_lifestyle_judging: Long = 0,
        var recommend_weight: String = "",
        var recommend_hashtag_numbering: String = "",
        var lifestyle_weight: String = "",
        var lifestyle_hashtag_numbering: String = "",
        var lifestyle_tip: String = ""
)