package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface TypificationHashtagStatisticsRepository : CrudRepository<TypificationHashtagStatistics, Long> {
    fun findByDateBeforeAndDateAfter(before: Instant, after: Instant): List<TypificationHashtagStatistics>
    fun findByDateBeforeAndDateAfterAndHashtagAndTypification(before: Instant, after: Instant, hashtag: String, typification: String): TypificationHashtagStatistics?
}