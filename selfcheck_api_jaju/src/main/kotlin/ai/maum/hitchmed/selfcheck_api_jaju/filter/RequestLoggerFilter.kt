package ai.maum.hitchmed.selfcheck_api_jaju.filter

import ai.maum.hitchmed.selfcheck_api_jaju.filter.extension.FilterUrlWrapper
import ai.maum.hitchmed.selfcheck_api_jaju.http.rdo.RequestInfo
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle.RequestRepository
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(1)
@Component
class RequestLoggerFilter(requestInfoFactory: ObjectFactory<RequestInfo>, requestRepository: RequestRepository)
    : FilterUrlWrapper(RequestLoggerFilterImpl(requestInfoFactory, requestRepository)) {
    init {
        allowRules {
            allow equals "/api/lifestyle/get"
            allow equals "/api/lifestyle/statistics/answer"
            allow equals "/api/lifestyle/statistics/hashtag"
            allow equals "/api/lifestyle/statistics/typification"
            allow equals "/api/lifestyle/statistics/typificationhashtag"
        }
    }
}