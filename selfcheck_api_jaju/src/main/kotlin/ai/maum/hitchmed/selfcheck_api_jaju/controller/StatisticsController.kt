package ai.maum.hitchmed.selfcheck_api_jaju.controller

import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.AnswerLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.RecommendationHashtagLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.TipHashtagLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.ExtendedController
import ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics.StatisticsAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics.StatisticsHashtag
import ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics.StatisticsTypification
import ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics.StatisticsTypificationHashtag
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsAnswerRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsHashtagRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsTypificationHashtagRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsTypificationRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics.AnswerStatisticsRepository
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics.HashtagStatisticsRepository
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics.TypificationHashtagStatisticsRepository
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics.TypificationStatisticsRepository
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class StatisticsController(
        var typificationStatisticsRepository: TypificationStatisticsRepository,
        var typificationHashtagStatisticsRepository: TypificationHashtagStatisticsRepository,
        var hashtagStatisticsRepository: HashtagStatisticsRepository,
        var answerStatisticsRepository: AnswerStatisticsRepository,
        var answerLibrary: AnswerLibrary,
        var recommendationHashtagLibrary: RecommendationHashtagLibrary,
        var tipHashtagLibrary: TipHashtagLibrary
) : ExtendedController {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Transactional
    @PostMapping("/api/lifestyle/statistics/typification")
    fun get(@RequestBody requestDto: StatisticsTypificationRequestDto) = handler {
        w controller this@StatisticsController
        w dto requestDto
        w handler StatisticsTypification(this@StatisticsController, requestDto)
    }

    @Transactional
    @PostMapping("/api/lifestyle/statistics/typificationhashtag")
    fun get(@RequestBody requestDto: StatisticsTypificationHashtagRequestDto) = handler {
        w controller this@StatisticsController
        w dto requestDto
        w handler StatisticsTypificationHashtag(this@StatisticsController, requestDto)
    }

    @Transactional
    @PostMapping("/api/lifestyle/statistics/hashtag")
    fun get(@RequestBody requestDto: StatisticsHashtagRequestDto) = handler {
        w controller this@StatisticsController
        w dto requestDto
        w handler StatisticsHashtag(this@StatisticsController, requestDto)
    }

    @Transactional
    @PostMapping("/api/lifestyle/statistics/answer")
    fun get(@RequestBody requestDto: StatisticsAnswerRequestDto) = handler {
        w controller this@StatisticsController
        w dto requestDto
        w handler StatisticsAnswer(this@StatisticsController, requestDto)
    }
}