package ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.controller.LifeStyleController
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.HandlerType
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.ResponseType
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.LifeStyleGetRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.LifeStyleGetResponseDto
import ai.maum.hitchmed.selfcheck_api_jaju.model.factory.UserAnswerFactory
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserChoiceAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserShortAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.AgeGroup
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class LifeStyleGet(controller: LifeStyleController, dto: LifeStyleGetRequestDto)
    : HandlerType<LifeStyleController, LifeStyleGetRequestDto>(controller, dto) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun invoke(): ResponseType {
        // 0. Aliases
        val uniqueId = dto.id
        val answers = dto.answers
        val answerLibrary = controller.answerLibrary
        val libAnswers = answerLibrary.library
        val libShortAnswers = answerLibrary.libShortAnswers
        val libChoiceAnswers = answerLibrary.libChoiceAnswers

        logger.info("@1 " + Instant.now().toString())

        // 1. 답변 파싱
        val userAnswers = UserAnswerFactory.of(answers)
        val choiceRecords = mutableListOf<AnswerRecord>()
        val shortRecords = mutableListOf<AnswerRecord>()

        for (answer in userAnswers) {
            if (answer.is_short) continue
            val choice = answer as UserChoiceAnswer
            choiceRecords.addAll(libChoiceAnswers.filter { it.answer in choice.answer_choice })
        }
        for (answer in userAnswers) {
            if (!answer.is_short) continue
            val short = answer as UserShortAnswer
            shortRecords.addAll(libShortAnswers.filter { it.answer % 100 == short.question })
        }
        val allRecords = choiceRecords + shortRecords

        logger.info("@2 " + Instant.now().toString())
        // 2. 개인정보 파싱
        val (gender, ageGroup) = controller.personalInformationParser.proc(userAnswers)

        // 3. 유효성 검사
        if (ageGroup !in AgeGroup.validRange)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid age")

        logger.info("@4 " + Instant.now().toString())
        // 4. 유형화 도출
        val typification = controller.typifier.proc(allRecords)

        logger.info("@5 " + Instant.now().toString())
        // 5. 해시태그 도출
        val (weightedRecommendationHashtags, weightedLifeStyleHashtags) = controller.hashtagCalculator.proc(choiceRecords)

        logger.info("@6 " + Instant.now().toString())
        // 6. 추천상품 도출
        val (solutionCareHashtags, productRecommendations, lifestyleRandom, healthRandom) = controller.productRecommender.proc(weightedRecommendationHashtags)

        logger.info("@7 " + Instant.now().toString())
        // 7. 건강/라이프스타일/심리팁 도출
        val (tipHashtags, tips) = controller.tipGenerator.proc(weightedLifeStyleHashtags)

        logger.info("@8,9 " + Instant.now().toString())
        GlobalScope.launch {
            // 8. 문진 기록
            controller.historyUploader.proc(uniqueId, answers, typification, solutionCareHashtags, productRecommendations, tips)

            // 9. 통계 기록
            controller.statisticsUploader.proc(gender, ageGroup, typification, solutionCareHashtags, tipHashtags, choiceRecords)
        }

        logger.info("@10 " + Instant.now().toString())
        // 10. 응답 작성
        val ret = LifeStyleGetResponseDto(
                typification = typification.number,
                recommendation_hashtag = solutionCareHashtags,
                solution_care = productRecommendations,
                lifestyle_random = lifestyleRandom,
                health_random = healthRandom,
                tips = tips
        )

        logger.info("@11 " + Instant.now().toString())
        // 11. 요청-응답 기록
        controller.requestInfoFactory.`object`.info.uniqueId = dto.id

        return ResponseEntity.ok(ret)
    }
}