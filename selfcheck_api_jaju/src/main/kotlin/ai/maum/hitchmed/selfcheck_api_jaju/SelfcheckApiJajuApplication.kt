package ai.maum.hitchmed.selfcheck_api_jaju

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SelfcheckApiJajuApplication

fun main(args: Array<String>) {
    runApplication<SelfcheckApiJajuApplication>(*args)
}
