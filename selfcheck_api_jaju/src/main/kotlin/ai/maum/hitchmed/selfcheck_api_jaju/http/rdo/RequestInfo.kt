package ai.maum.hitchmed.selfcheck_api_jaju.http.rdo

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle.Request
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component

// Request Data Object
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
data class RequestInfo(
        var info: Request = Request()
)