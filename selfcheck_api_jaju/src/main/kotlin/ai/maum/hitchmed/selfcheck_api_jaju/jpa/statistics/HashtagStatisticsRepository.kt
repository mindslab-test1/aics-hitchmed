package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface HashtagStatisticsRepository : CrudRepository<HashtagStatistics, Long> {
    fun findByDateBeforeAndDateAfter(before: Instant, after: Instant): List<HashtagStatistics>
    fun findByDateBeforeAndDateAfterAndAgeGroupAndGenderAndHashtag(before: Instant, after: Instant, ageGroup: String, gender: String, hashtag: String): HashtagStatistics?
}