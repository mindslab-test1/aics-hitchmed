package ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle

import org.springframework.data.repository.CrudRepository

interface ProductRepository : CrudRepository<Product, Long> {
}