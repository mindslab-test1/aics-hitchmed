package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.TipHashtagLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.TipItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.TipGeneratorException
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.WeightedHashtag
import org.springframework.stereotype.Service

@Service
class TipGenerator(
        var tipHashtagLibrary: TipHashtagLibrary
) {
    fun proc(tipHashtagList: List<WeightedHashtag>): Pair<List<String>, List<TipItemDto>> {
        val library = tipHashtagLibrary.library

        val descending = tipHashtagList.sortedByDescending { it.weight }
        val health = mutableListOf<String>()
        val others = mutableListOf<String>()

        for (each in descending) {
            val category = library.find { it.hashtag_numbering == each.hashtag_numbering }?.category
                    ?: throw TipGeneratorException(1, "Invalid hashtag or category")

            when (category) {
                "h" -> health.add(each.hashtag_numbering)
                "l" -> others.add(each.hashtag_numbering)
                else -> throw TipGeneratorException(2, "Invalid category")
            }
        }

        var healthCount = 0
        var othersCount = 0
        if (health.size > 0 && others.size > 0) {
            healthCount = 1
            othersCount = 1
        } else if (health.size <= 0) {
            healthCount = 0
            othersCount = others.size
        } else {
            healthCount = health.size
            othersCount = 0
        }
        if (healthCount > 2) healthCount = 2
        if (othersCount > 2) othersCount = 2

        val resultHashtags = mutableListOf<String>()
        val resultTipItems = mutableListOf<TipItemDto>()
        for (i in 0 until healthCount) {
            resultHashtags.add(health[i])
            val tipItem = TipItemDto(
                    is_lifestyle_tip = false,
                    hashtag = health[i],
                    content = library.find { it.hashtag_numbering == health[i] }?.content ?: ""
            )
            resultTipItems.add(tipItem)
        }
        for (i in 0 until othersCount) {
            resultHashtags.add(others[i])
            val tipItem = TipItemDto(
                    is_lifestyle_tip = true,
                    hashtag = others[i],
                    content = library.find { it.hashtag_numbering == others[i] }?.content ?: ""
            )
            resultTipItems.add(tipItem)
        }

        return Pair(resultHashtags, resultTipItems)
    }
}