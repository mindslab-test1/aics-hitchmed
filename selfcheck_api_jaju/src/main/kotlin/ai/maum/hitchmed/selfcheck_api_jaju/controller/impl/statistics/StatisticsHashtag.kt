package ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.controller.StatisticsController
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.HandlerType
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.ResponseType
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsHashtagRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsHashtagResponseDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsHashtagResponseItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.AgeGroup
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.Gender
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class StatisticsHashtag(controller: StatisticsController, dto: StatisticsHashtagRequestDto)
    : HandlerType<StatisticsController, StatisticsHashtagRequestDto>(controller, dto) {

    override fun invoke(): ResponseType {
        val date = dto.date
        val after = Instant.parse("${date.subSequence(0, 4)}-${date.subSequence(4, 6)}-${date.subSequence(6, 8)}T00:00:00.00Z").minusMillis(1)
        val before = after.plusMillis(1000 * 60 * 60 * 24 + 1)

        // 필요한 날짜는 UTC+9의 관점에서 0시~24시
        // 즉 UTC+0의 관점에서 -9시~15시를 가져오면 됨
        val utcAfter = after.minusMillis(1000 * 60 * 60 * 9)
        val utcBefore = before.minusMillis(1000 * 60 * 60 * 9)

        val hashtagStatistics = controller.hashtagStatisticsRepository.findByDateBeforeAndDateAfter(utcBefore, utcAfter)

        val result = mutableListOf<StatisticsHashtagResponseItemDto>()
        for (hashtag in controller.tipHashtagLibrary.library) {
            for (gender in Gender.validRange) {
                val item = StatisticsHashtagResponseItemDto(
                        tag = hashtag.hashtag_numbering.toUpperCase(),
                        gender = gender.toString(),
                        age0_24 = 0,
                        age25_34 = 0,
                        age35_44 = 0,
                        age45_54 = 0,
                        age55_64 = 0,
                        age65_99 = 0
                )

                result.add(item)
            }
        }
        for (hashtag in controller.recommendationHashtagLibrary.library) {
            // skip if NOT USED
            if (hashtag.category == RecommendationHashtagRecord.Category.NOT_USED)
                continue

            for (gender in Gender.validRange) {
                val item = StatisticsHashtagResponseItemDto(
                        tag = hashtag.hashtag_numbering.toUpperCase(),
                        gender = gender.toString(),
                        age0_24 = 0,
                        age25_34 = 0,
                        age35_44 = 0,
                        age45_54 = 0,
                        age55_64 = 0,
                        age65_99 = 0
                )

                result.add(item)
            }
        }

        for (cell in hashtagStatistics) {
            val item = result.find { it.tag == cell.hashtag!!.toUpperCase() && it.gender == cell.gender!! }
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "find null")

            when (AgeGroup.fromEntity(cell.ageGroup!!)) {
                AgeGroup.R0_24 -> item.age0_24 = cell.count!!
                AgeGroup.R25_34 -> item.age25_34 = cell.count!!
                AgeGroup.R35_44 -> item.age35_44 = cell.count!!
                AgeGroup.R45_54 -> item.age45_54 = cell.count!!
                AgeGroup.R55_64 -> item.age55_64 = cell.count!!
                AgeGroup.R65_99 -> item.age65_99 = cell.count!!
                else -> continue
            }
        }

        return ResponseEntity.ok(StatisticsHashtagResponseDto(result))
    }
}