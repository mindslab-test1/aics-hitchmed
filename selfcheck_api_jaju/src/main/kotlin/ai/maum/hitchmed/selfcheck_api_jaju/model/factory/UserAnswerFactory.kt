package ai.maum.hitchmed.selfcheck_api_jaju.model.factory

import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.AnswerItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserChoiceAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserShortAnswer

class UserAnswerFactory private constructor() {
    companion object {
        fun of(dto: List<AnswerItemDto>): List<UserAnswer> = dto.map { of(it) }
        fun of(dto: AnswerItemDto): UserAnswer =
                if (dto.is_short)
                    UserShortAnswer(
                            dto.question,
                            dto.answer_short!!
                    )
                else
                    UserChoiceAnswer(
                            dto.question,
                            dto.answer_choice!!
                    )
    }
}