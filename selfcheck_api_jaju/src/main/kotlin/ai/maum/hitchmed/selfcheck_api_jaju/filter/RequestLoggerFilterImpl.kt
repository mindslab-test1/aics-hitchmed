package ai.maum.hitchmed.selfcheck_api_jaju.filter

import ai.maum.hitchmed.selfcheck_api_jaju.filter.extension.FilterImpl
import ai.maum.hitchmed.selfcheck_api_jaju.http.rdo.RequestInfo
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle.RequestRepository
import org.springframework.beans.factory.ObjectFactory
import java.io.IOException
import java.time.Instant
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class RequestLoggerFilterImpl(
        var requestInfoFactory: ObjectFactory<RequestInfo>,
        var requestRepository: RequestRepository,
) : FilterImpl {
    @Throws(IOException::class, ServletException::class)
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val info = requestInfoFactory.`object`.info
        info.uniqueId // to be set by handler
        info.remoteHost = request.remoteHost
        info.requestMethod = request.method
        info.requestPayloadSize = request.contentLengthLong
        info.requestTime = Instant.now()
        info.requestContentType = request.contentType
        info.pathAbsolute = request.requestURI

        try {
            chain.doFilter(request, response)
            info.responseStatusCode = response.status.toLong()
        } catch (e: Exception) {
            info.responseStatusCode = 500L
            throw e
        } finally {
            if (info.uniqueId == null)
                info.uniqueId = "NULL"
            info.responseTime = Instant.now()
            requestRepository.save(info)
        }
    }
}