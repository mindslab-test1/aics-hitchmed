package ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Tip : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "TIP_SEQ_GEN",
            sequenceName = "TIP_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIP_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var uniqueId: String? = null

    @Column(nullable = false)
    var isLifeStyle: Boolean? = null

    @Column(nullable = false)
    var hashtag: String? = null
}