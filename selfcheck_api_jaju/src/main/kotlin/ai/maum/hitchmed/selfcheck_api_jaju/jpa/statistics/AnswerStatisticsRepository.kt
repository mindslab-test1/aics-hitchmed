package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface AnswerStatisticsRepository : CrudRepository<AnswerStatistics, Long> {
    fun findByDateBeforeAndDateAfter(before: Instant, after: Instant): List<AnswerStatistics>
    fun findByDateBeforeAndDateAfterAndAnswer(before: Instant, after: Instant, answer: Long): AnswerStatistics?
}