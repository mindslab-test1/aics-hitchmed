package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.WeightedHashtag
import kotlinx.serialization.Serializable

@Serializable
data class AnswerRecord(
        var answer: Long,
        var group_scoring_fitness_health: Long,
        var group_scoring_fatigue_sleep: Long,
        var group_scoring_stress_immunity: Long,
        var group_scoring_mbti_energy_extroversion: Long,
        var group_scoring_mbti_information_sensing: Long,
        var group_scoring_mbti_decision_thinking: Long,
        var group_scoring_mbti_lifestyle_judging: Long,
        var recommendation: List<WeightedHashtag>,
        var lifestyle: List<WeightedHashtag>
)