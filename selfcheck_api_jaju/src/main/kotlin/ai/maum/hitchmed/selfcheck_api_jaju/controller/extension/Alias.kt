package ai.maum.hitchmed.selfcheck_api_jaju.controller.extension

import org.springframework.http.ResponseEntity

typealias ResponseType = ResponseEntity<Any>
typealias HandlerMethod = () -> ResponseType

abstract class HandlerType<Controller, Dto>(val controller: Controller, val dto: Dto) : HandlerMethod

@DslMarker
annotation class HandlerMarker

class Alias