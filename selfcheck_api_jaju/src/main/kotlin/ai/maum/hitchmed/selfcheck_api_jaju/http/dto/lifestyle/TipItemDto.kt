package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle

import kotlinx.serialization.Serializable

@Serializable
data class TipItemDto(
        var is_lifestyle_tip: Boolean,
        var hashtag: String,
        var content: String
)