package ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Answer : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "RESPONSE_SEQ_GEN",
            sequenceName = "RESPONSE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RESPONSE_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var uniqueId: String? = null

    @Column(nullable = false)
    var question: Long? = null

    @Column(nullable = true)
    var answerChoice: String? = null

    @Column(nullable = true)
    var answerShort: String? = null
}