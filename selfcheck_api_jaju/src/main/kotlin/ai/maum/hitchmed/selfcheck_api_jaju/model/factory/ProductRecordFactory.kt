package ai.maum.hitchmed.selfcheck_api_jaju.model.factory

import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.ProductRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.ProductRecordRaw

class ProductRecordFactory private constructor() {
    companion object {
        fun of(from: ProductRecordRaw): ProductRecord = ProductRecord(
                category = from.category,
                hashtag_name = from.hashtag_name,
                hashtag_numbering = from.hashtag_numbering,
                id = from.id,
                name = from.name,
                cost = from.cost.replace(",", "").replace(" ", "").toLong(),
                price = from.price.replace(",", "").replace(" ", "").toLong(),
                description = from.description,
                image = from.image,
                url = from.url,
                priority = from.priority
        )
    }
}