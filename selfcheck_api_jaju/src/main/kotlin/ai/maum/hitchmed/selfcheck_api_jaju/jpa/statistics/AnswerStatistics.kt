package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class AnswerStatistics : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "ANSWER_STATS_SEQ_GEN",
            sequenceName = "ANSWER_STATS_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANSWER_STATS_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var answer: Long? = null

    @Column(nullable = false)
    var count: Long? = null

    @Column(nullable = false, name = "\"SDATE\"")
    var date: Instant? = null
}