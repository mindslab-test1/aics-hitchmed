package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics

import kotlinx.serialization.Serializable

@Serializable
data class StatisticsAnswerResponseItemDto(
        var anwno: Long,
        var count: Long
)