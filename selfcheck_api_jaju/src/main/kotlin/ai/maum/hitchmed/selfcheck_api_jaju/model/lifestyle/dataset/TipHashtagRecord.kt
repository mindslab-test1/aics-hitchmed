package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import kotlinx.serialization.Serializable

@Serializable
data class TipHashtagRecord(
        var hashtag_numbering: String = "",
        var hashtag_name: String = "",
        var category: String = "",
        var content: String = ""
)