package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle

import kotlinx.serialization.Serializable

@Serializable
data class ProductItemDto(
        var hashtag: String,
        var image: String,
        var link: String,
        var name: String,
        var code: String,
        var description: String,
        var cost: Long? = null,
        var price: Long? = null
)