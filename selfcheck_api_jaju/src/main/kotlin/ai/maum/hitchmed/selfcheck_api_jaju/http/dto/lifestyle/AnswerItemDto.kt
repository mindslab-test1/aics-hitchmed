package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle

import kotlinx.serialization.Serializable

@Serializable
data class AnswerItemDto(
        var is_short: Boolean,
        var question: Long,

        var answer_choice: List<Long>? = null,
        var answer_short: String? = null
) {
    init {
        if (is_short) {
            requireNotNull(answer_short)
            require(answer_choice == null)
        }
        if (!is_short) {
            require(answer_short == null)
            requireNotNull(answer_choice)
        }
    }
}