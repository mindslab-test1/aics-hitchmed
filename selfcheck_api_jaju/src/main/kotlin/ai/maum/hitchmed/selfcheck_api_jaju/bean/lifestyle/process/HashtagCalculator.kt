package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.WeightedHashtag
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecord
import org.springframework.stereotype.Service

@Service
class HashtagCalculator {
    fun proc(answers: List<AnswerRecord>): Pair<List<WeightedHashtag>, List<WeightedHashtag>> {
        val answeredRecommendations = mutableListOf<WeightedHashtag>()
        val answeredLifeStyles = mutableListOf<WeightedHashtag>()
        for (answer in answers) {
            for (weighted in answer.recommendation) {
                val already = answeredRecommendations.find { it.hashtag_numbering == weighted.hashtag_numbering }
                already ?: answeredRecommendations.add(weighted)
                already?.let {
                    val newItem = WeightedHashtag(already.weight + weighted.weight, weighted.hashtag_numbering)
                    answeredRecommendations.remove(already)
                    answeredRecommendations.add(newItem)
                }
            }
            for (weighted in answer.lifestyle) {
                val already = answeredLifeStyles.find { it.hashtag_numbering == weighted.hashtag_numbering }
                already ?: answeredLifeStyles.add(weighted)
                already?.let {
                    val newItem = WeightedHashtag(already.weight + weighted.weight, weighted.hashtag_numbering)
                    answeredLifeStyles.remove(already)
                    answeredLifeStyles.add(newItem)
                }
            }
        }
        answeredRecommendations.sortByDescending { it.weight }
        answeredLifeStyles.sortByDescending { it.weight }

        return Pair(answeredRecommendations, answeredLifeStyles)
    }
}