package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
@Table(name = "\"TH_STATISICS\"")
class TypificationHashtagStatistics : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "TH_STATS_SEQ_GEN",
            sequenceName = "TH_STATS_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TH_STATS_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var typification: String? = null

    @Column(nullable = false)
    var hashtag: String? = null

    @Column(nullable = false)
    var count: Long? = null

    @Column(nullable = false, name = "\"SDATE\"")
    var date: Instant? = null
}