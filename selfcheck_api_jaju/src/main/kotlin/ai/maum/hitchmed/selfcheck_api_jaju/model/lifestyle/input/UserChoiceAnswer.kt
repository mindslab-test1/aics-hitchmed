package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input

class UserChoiceAnswer(question: Long, var answer_choice: List<Long>) : UserAnswer(false, question)