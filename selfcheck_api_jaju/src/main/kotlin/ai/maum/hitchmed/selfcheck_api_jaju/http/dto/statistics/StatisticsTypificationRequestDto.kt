package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics

import kotlinx.serialization.Serializable

@Serializable
data class StatisticsTypificationRequestDto(
        var date: String
)