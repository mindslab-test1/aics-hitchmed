package ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.controller.StatisticsController
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.HandlerType
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.ResponseType
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsTypificationHashtagRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsTypificationHashtagResponseDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics.StatisticsTypificationHashtagResponseItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.Typification
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecord
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class StatisticsTypificationHashtag(controller: StatisticsController, dto: StatisticsTypificationHashtagRequestDto)
    : HandlerType<StatisticsController, StatisticsTypificationHashtagRequestDto>(controller, dto) {

    // TODO: 나중에 service로 돌린다
    override fun invoke(): ResponseType {
        val date = dto.date
        val after = Instant.parse("${date.subSequence(0, 4)}-${date.subSequence(4, 6)}-${date.subSequence(6, 8)}T00:00:00.00Z").minusMillis(1)
        val before = after.plusMillis(1000 * 60 * 60 * 24 + 1)

        // 필요한 날짜는 UTC+9의 관점에서 0시~24시
        // 즉 UTC+0의 관점에서 -9시~15시를 가져오면 됨
        val utcAfter = after.minusMillis(1000 * 60 * 60 * 9)
        val utcBefore = before.minusMillis(1000 * 60 * 60 * 9)

        val typificationHashtagStatistics = controller.typificationHashtagStatisticsRepository.findByDateBeforeAndDateAfter(utcBefore, utcAfter)

        val result = mutableListOf<StatisticsTypificationHashtagResponseItemDto>()
        for (hashtag in controller.tipHashtagLibrary.library) {
            val item = StatisticsTypificationHashtagResponseItemDto(
                    tag = hashtag.hashtag_numbering.toUpperCase(),
                    g001 = 0,
                    g002 = 0,
                    g003 = 0,
                    g004 = 0,
                    g005 = 0,
                    g006 = 0,
                    g007 = 0,
                    g008 = 0,
                    g009 = 0,
                    g010 = 0,
                    g011 = 0,
                    g012 = 0,
                    g013 = 0,
                    g014 = 0,
                    g015 = 0,
                    g016 = 0,
                    g017 = 0,
                    g018 = 0,
                    g019 = 0,
                    g020 = 0,
                    g021 = 0,
                    g022 = 0,
                    g023 = 0,
                    g024 = 0
            )

            result.add(item)
        }
        for (hashtag in controller.recommendationHashtagLibrary.library) {
            // skip if NOT USED
            if (hashtag.category == RecommendationHashtagRecord.Category.NOT_USED)
                continue

            val item = StatisticsTypificationHashtagResponseItemDto(
                    tag = hashtag.hashtag_numbering.toUpperCase(),
                    g001 = 0,
                    g002 = 0,
                    g003 = 0,
                    g004 = 0,
                    g005 = 0,
                    g006 = 0,
                    g007 = 0,
                    g008 = 0,
                    g009 = 0,
                    g010 = 0,
                    g011 = 0,
                    g012 = 0,
                    g013 = 0,
                    g014 = 0,
                    g015 = 0,
                    g016 = 0,
                    g017 = 0,
                    g018 = 0,
                    g019 = 0,
                    g020 = 0,
                    g021 = 0,
                    g022 = 0,
                    g023 = 0,
                    g024 = 0
            )

            result.add(item)
        }

        for (cell in typificationHashtagStatistics) {
            val item = result.find { it.tag == cell.hashtag }
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "find null")
            when (Typification.of(cell.typification!!.toUpperCase())) {
                Typification.g001 -> item.g001 = cell.count!!
                Typification.g002 -> item.g002 = cell.count!!
                Typification.g003 -> item.g003 = cell.count!!
                Typification.g004 -> item.g004 = cell.count!!
                Typification.g005 -> item.g005 = cell.count!!
                Typification.g006 -> item.g006 = cell.count!!
                Typification.g007 -> item.g007 = cell.count!!
                Typification.g008 -> item.g008 = cell.count!!
                Typification.g009 -> item.g009 = cell.count!!
                Typification.g010 -> item.g010 = cell.count!!
                Typification.g011 -> item.g011 = cell.count!!
                Typification.g012 -> item.g012 = cell.count!!
                Typification.g013 -> item.g013 = cell.count!!
                Typification.g014 -> item.g014 = cell.count!!
                Typification.g015 -> item.g015 = cell.count!!
                Typification.g016 -> item.g016 = cell.count!!
                Typification.g017 -> item.g017 = cell.count!!
                Typification.g018 -> item.g018 = cell.count!!
                Typification.g019 -> item.g019 = cell.count!!
                Typification.g020 -> item.g020 = cell.count!!
                Typification.g021 -> item.g021 = cell.count!!
                Typification.g022 -> item.g022 = cell.count!!
                Typification.g023 -> item.g023 = cell.count!!
                Typification.g024 -> item.g024 = cell.count!!
            }
        }

        return ResponseEntity.ok(StatisticsTypificationHashtagResponseDto(result))
    }
}