package ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Result : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "RESULT_SEQ_GEN",
            sequenceName = "RESULT_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RESULT_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var uniqueId: String? = null

    @Column(nullable = false)
    var typification: String? = null

    @Column(nullable = false)
    var recommendation: String? = null
}