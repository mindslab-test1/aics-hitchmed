package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.PersonalInformationParserException
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserChoiceAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.input.UserShortAnswer
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.AgeGroup
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.Gender
import org.springframework.stereotype.Service

@Service
class PersonalInformationParser {
    fun proc(userAnswers: List<UserAnswer>): Pair<Gender, AgeGroup> {
        val genderChoiceAnswer = userAnswers.find { it.question == 200L }
                ?: throw PersonalInformationParserException(1, "Gender not answered")
        val genderChoice = genderChoiceAnswer as UserChoiceAnswer
        val genderString = when (genderChoice.answer_choice[0]) {
            201L -> "여"
            202L -> "남"
            203L -> "선택안함"
            else -> throw PersonalInformationParserException(2, "Invalid gender input")
        }
        val gender = Gender.fromEntity(genderString)
        val ageAnswer = userAnswers.find { it.question == 300L }
                ?: throw PersonalInformationParserException(3, "Age not answered")
        val ageGroup = AgeGroup.fromBirth((ageAnswer as UserShortAnswer).answer_short)

        if (ageGroup == AgeGroup.ERROR)
            throw PersonalInformationParserException(4, "Invalid age input")

        return Pair(gender, ageGroup)
    }
}