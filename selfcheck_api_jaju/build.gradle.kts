import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// Kotlinx dependencies
val serializationVersion = "1.0.1"
val coroutinesVersion = "1.4.1"

plugins {
    application
    id("org.springframework.boot") version "2.4.0"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.10"
    kotlin("plugin.spring") version "1.4.10"
    kotlin("plugin.jpa") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.4.10"
}

allOpen {
    annotation("ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen")
}

group = "ai.maum.hitchmed"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11
application.mainClassName = "ai.maum.hitchmed.selfcheck_api_jaju.SelfcheckApiJajuApplicationKt"

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()

    //ojdbc
    maven(url = "https://code.lds.org/nexus/content/groups/main-repo")
    //lombok
    maven(url = "https://plugins.gradle.org/m2/")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    runtimeOnly("com.h2database:h2")
    implementation(group = "com.oracle.database.jdbc", name = "ojdbc10", version = "19.8.0.0")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

configurations {
    all {
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-json")
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
