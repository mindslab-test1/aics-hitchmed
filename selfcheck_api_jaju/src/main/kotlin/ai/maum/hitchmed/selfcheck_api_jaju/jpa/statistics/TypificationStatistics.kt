package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class TypificationStatistics : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "TYPIFICATION_STATS_SEQ_GEN",
            sequenceName = "TYPIFICATION_STATS_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TYPIFICATION_STATS_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var gender: String? = null

    @Column(nullable = false)
    var ageGroup: String? = null

    @Column(nullable = false)
    var typification: String? = null

    @Column(nullable = false)
    var count: Long? = null

    @Column(nullable = false, name = "\"SDATE\"")
    var date: Instant? = null
}