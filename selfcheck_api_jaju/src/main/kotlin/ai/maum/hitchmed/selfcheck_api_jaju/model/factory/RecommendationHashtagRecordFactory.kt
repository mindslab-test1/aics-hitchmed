package ai.maum.hitchmed.selfcheck_api_jaju.model.factory

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.RecommendationHashtagRecordFactoryException
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecordRaw

class RecommendationHashtagRecordFactory {
    companion object {
        fun of(from: RecommendationHashtagRecordRaw): RecommendationHashtagRecord =
                RecommendationHashtagRecord(
                        hashtag_numbering = from.number,
                        hashtag_name = from.hashtag,
                        category = when (from.category) {
                            "h" -> RecommendationHashtagRecord.Category.HEALTH
                            "l" -> RecommendationHashtagRecord.Category.LIFESTYLE
                            "b" -> RecommendationHashtagRecord.Category.MENTAL
                            "n" -> RecommendationHashtagRecord.Category.NOT_USED
                            else -> throw RecommendationHashtagRecordFactoryException(1, "Invalid category")
                        }
                )
    }
}