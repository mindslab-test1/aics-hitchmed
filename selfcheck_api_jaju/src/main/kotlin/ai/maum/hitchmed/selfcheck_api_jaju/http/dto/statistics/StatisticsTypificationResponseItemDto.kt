package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics

import kotlinx.serialization.Serializable

@Serializable
data class StatisticsTypificationResponseItemDto(
        var age: String,
        var gender: String,
        var g001: Long,
        var g002: Long,
        var g003: Long,
        var g004: Long,
        var g005: Long,
        var g006: Long,
        var g007: Long,
        var g008: Long,
        var g009: Long,
        var g010: Long,
        var g011: Long,
        var g012: Long,
        var g013: Long,
        var g014: Long,
        var g015: Long,
        var g016: Long,
        var g017: Long,
        var g018: Long,
        var g019: Long,
        var g020: Long,
        var g021: Long,
        var g022: Long,
        var g023: Long,
        var g024: Long,
)