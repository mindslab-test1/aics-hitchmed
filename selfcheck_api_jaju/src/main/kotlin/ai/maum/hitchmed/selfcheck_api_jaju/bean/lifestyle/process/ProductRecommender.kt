package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.ProductLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.RecommendationHashtagLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.ProductItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.ProductRecommenderException
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.WeightedHashtag
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecord
import org.springframework.stereotype.Service

@Service
class ProductRecommender(
        var productLibrary: ProductLibrary,
        var recommendationHashtagLibrary: RecommendationHashtagLibrary
) {
    fun proc(recommendationHashtagList: List<WeightedHashtag>): ProductRecommendations {
        val products = productLibrary.library
        val hashtags = recommendationHashtagLibrary.library

        val hashtagResult = mutableListOf<String>()

        val descending = recommendationHashtagList.sortedByDescending { it.weight }.toMutableList()
        val beauty = mutableListOf<WeightedHashtag>()
        val lifeStyle = mutableListOf<WeightedHashtag>()
        val health = mutableListOf<WeightedHashtag>()

        // split hashtags by category
        for (each in descending) {
            // check if hashtag not used currently
            val hashtag = hashtags.find { each.hashtag_numbering == it.hashtag_numbering }
                    ?: throw ProductRecommenderException(1, "Invalid numbering")
            if (hashtag.category == RecommendationHashtagRecord.Category.NOT_USED)
                continue

            // check product category
            val category = products.find { each.hashtag_numbering == it.hashtag_numbering }?.category
                    ?: throw ProductRecommenderException(2, "Invalid numbering or category")
            when (category) {
                "뷰티" -> beauty.add(each)
                "라이프스타일" -> lifeStyle.add(each)
                "건강" -> health.add(each)
                else -> throw ProductRecommenderException(3, "Invalid category")
            }
        }

        /* 상품 추천(weight 기반)
        ########################################## 원문 #########################################
        **뷰티, 라이프스타일, 건강 카테고리로 분리하여 해시태그 순위를 메김**
        **해시태그 순서 : 라이프스타일 1순위 - 라이프스타일 2순위 - 건강 1순위 - 건강 2순위 - 뷰티 1순위**
        **같은 카테고리 내 동순위 해시태그가 발생하는 경우 랜덤으로 순위 결정**
        뷰티가 1개 이상인 경우
         -> 뷰티 1개, 라이프스타일 2개, 건강 2개 노출
        뷰티가 0개인 경우
         - 3순위 라이프스타일 해시태그 웨이트값 > 3순위 건강 해시태그 웨이트값
             -> 라이프스타일 3개, 건강 2개
         - 3순위 라이프스타일 해시태그 웨이트값 < 3순위 건강 해시태그 웨이트값
             -> 라이프스타일 2개, 건강 3개
         - 3순위 라이프스타일 해시태그 웨이트값 = 3순위 건강 해시태그 웨이트값
             -> 라이프스타일 3개, 건강 2개
        ########################################## 원문 끝 ######################################
        여기에 해시태그 개수가 모자라는 경우 있는 만큼만 넣는 것으로 하였음(최대 2개)
         */
        val lifeStyleRandomCandidates = mutableListOf<WeightedHashtag>()
        val healthRandomCandidates = mutableListOf<WeightedHashtag>()
        if (beauty.isNotEmpty()) {
            if (lifeStyle.size > 0) {
                hashtagResult.add(lifeStyle[0].hashtag_numbering)
                lifeStyleRandomCandidates.add(lifeStyle[0])
            }
            if (lifeStyle.size > 1) {
                hashtagResult.add(lifeStyle[1].hashtag_numbering)
                lifeStyleRandomCandidates.add(lifeStyle[1])
            }
            if (health.size > 0) {
                hashtagResult.add(health[0].hashtag_numbering)
                healthRandomCandidates.add(health[0])
            }
            if (health.size > 1) {
                hashtagResult.add(health[1].hashtag_numbering)
                healthRandomCandidates.add(health[1])
            }
            hashtagResult.add(beauty[0].hashtag_numbering)
        } else {
            if (lifeStyle.size > 2 && health.size > 2) {
                if (lifeStyle[2].weight >= health[2].weight) {
                    hashtagResult.add(lifeStyle[0].hashtag_numbering)
                    hashtagResult.add(lifeStyle[1].hashtag_numbering)
                    hashtagResult.add(lifeStyle[2].hashtag_numbering)
                    hashtagResult.add(health[0].hashtag_numbering)
                    hashtagResult.add(health[1].hashtag_numbering)
                    lifeStyleRandomCandidates.addAll(listOf(lifeStyle[0], lifeStyle[1], lifeStyle[2]))
                    healthRandomCandidates.addAll(listOf(health[0], health[1]))
                } else {
                    hashtagResult.add(lifeStyle[0].hashtag_numbering)
                    hashtagResult.add(lifeStyle[1].hashtag_numbering)
                    hashtagResult.add(health[0].hashtag_numbering)
                    hashtagResult.add(health[1].hashtag_numbering)
                    hashtagResult.add(health[2].hashtag_numbering)
                    lifeStyleRandomCandidates.addAll(listOf(lifeStyle[0], lifeStyle[1]))
                    healthRandomCandidates.addAll(listOf(health[0], health[1], health[2]))
                }
            } else {
                if (lifeStyle.size > 0) {
                    hashtagResult.add(lifeStyle[0].hashtag_numbering)
                    lifeStyleRandomCandidates.add(lifeStyle[0])
                }
                if (lifeStyle.size > 1) {
                    hashtagResult.add(lifeStyle[1].hashtag_numbering)
                    lifeStyleRandomCandidates.add(lifeStyle[1])
                }
                //if (lifeStyle.size > 2) hashtagResult.add(lifeStyle[2].hashtag_numbering)
                if (health.size > 0) {
                    hashtagResult.add(health[0].hashtag_numbering)
                    healthRandomCandidates.add(health[0])
                }
                if (health.size > 1) {
                    hashtagResult.add(health[1].hashtag_numbering)
                    healthRandomCandidates.add(health[1])
                }
                //if (health.size > 2) hashtagResult.add(health[2].hashtag_numbering)
            }
        }

        val fixedHashtags = hashtagResult.toList()
        val ret = mutableListOf<ProductItemDto>()

        for (each in fixedHashtags) {
            val matchingProducts = products.filter { it.hashtag_numbering == each }
            for (product in matchingProducts) {
                val dto = ProductItemDto(
                        code = product.id,
                        cost = product.cost,
                        description = product.description,
                        hashtag = product.hashtag_numbering,
                        image = product.image,
                        name = product.name,
                        price = product.price,
                        link = product.url
                )

                ret.add(dto)
            }
        }

        /* 상품 추천(random)
products.filter { it.hashtag_numbering == each }
         */
        val lifeStyleRandomHashtag = lifeStyleRandomCandidates.random()
        val lifeStyleProducts = products.filter { it.hashtag_numbering == lifeStyleRandomHashtag.hashtag_numbering }
        val lifeStyleRandom = if (lifeStyleProducts.isNotEmpty()) {
            val product = lifeStyleProducts.random()
            ProductItemDto(
                    code = product.id,
                    cost = product.cost,
                    description = product.description,
                    hashtag = product.hashtag_numbering,
                    image = product.image,
                    name = product.name,
                    price = product.price,
                    link = product.url
            )
        } else
            ProductItemDto(code = "", cost = 0, description = "", hashtag = "", image = "", name = "", price = 0, link = "")

        val healthRandomHashtag = healthRandomCandidates.random()
        val healthProducts = products.filter { it.hashtag_numbering == healthRandomHashtag.hashtag_numbering }
        val healthRandom = if (healthProducts.isNotEmpty()) {
            val product = healthProducts.random()
            ProductItemDto(
                    code = product.id,
                    cost = product.cost,
                    description = product.description,
                    hashtag = product.hashtag_numbering,
                    image = product.image,
                    name = product.name,
                    price = product.price,
                    link = product.url
            )
        } else
            ProductItemDto(code = "", cost = 0, description = "", hashtag = "", image = "", name = "", price = 0, link = "")

        return ProductRecommendations(fixedHashtags, ret, lifeStyleRandom, healthRandom)
    }

    data class ProductRecommendations(
            val solutionCareHashtags: List<String>,
            val productRecommendations: List<ProductItemDto>,
            val lifestyleRandom: ProductItemDto,
            val healthRandom: ProductItemDto
    )
}