package ai.maum.hitchmed.selfcheck_api_jaju.filter.extension

import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

interface FilterImpl {
    fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain)
}