package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import kotlinx.serialization.Serializable

@Serializable
data class ProductRecordRaw(
        var category: String = "",
        var hashtag_name: String = "",
        var hashtag_numbering: String = "",
        var id: String = "",
        var name: String = "",
        var cost: String = "",
        var price: String = "",
        var description: String = "",
        var image: String = "",
        var url: String = "",
        var priority: Long = 0L
)