package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle

import kotlinx.serialization.Serializable

@Serializable
data class LifeStyleGetRequestDto(
        var id: String,
        var answers: List<AnswerItemDto>
)