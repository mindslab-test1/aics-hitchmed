package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle

import kotlinx.serialization.Serializable

@Serializable
data class WeightedHashtag(
        var weight: Double,
        var hashtag_numbering: String
)