package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle

import kotlinx.serialization.Serializable

@Serializable
data class LifeStyleGetResponseDto(
        var typification: String,
        var recommendation_hashtag: List<String>,
        var tips: List<TipItemDto>,
        var solution_care: List<ProductItemDto>,
        var lifestyle_random: ProductItemDto,
        var health_random: ProductItemDto
)