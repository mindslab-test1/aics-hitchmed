package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.HealthGroup
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.Principal
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.Typification
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecord
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class Typifier {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun proc(answers: List<AnswerRecord>): Typification {
        val fitnessHealth = answers.sumByLong { it.group_scoring_fitness_health }
        val fatigueSleep = answers.sumByLong { it.group_scoring_fatigue_sleep }
        val stressImmunity = answers.sumByLong { it.group_scoring_stress_immunity }

        val e = answers.map { it.group_scoring_mbti_energy_extroversion }.sum()
        val s = answers.map { it.group_scoring_mbti_information_sensing }.sum()
        val t = answers.map { it.group_scoring_mbti_decision_thinking }.sum()
        val j = answers.map { it.group_scoring_mbti_lifestyle_judging }.sum()

        val extroversion = e > 0
        val sensing = s > 0
        val thinking = t > 0
        val judging = j > 0

        for (answer in answers)
            logger.debug("${answer.answer}: ${answer.group_scoring_mbti_decision_thinking}")

        logger.debug("fitnessHealth: $fitnessHealth, fatigueSleep: $fatigueSleep, stressImmunity: $stressImmunity")
        logger.debug("extroversion: $e, sensing: $s, thinking: $t, judging: $j")

        val healthGroup = HealthGroup.of(fitnessHealth, fatigueSleep, stressImmunity)
        val principal = Principal.of(extroversion, sensing, thinking, judging)

        logger.debug("healthGroup: ${healthGroup.name}")
        logger.debug("principal: ${principal.name}")

        return Typification.of(healthGroup, principal)
    }
}

inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
    var sum = 0L
    for (element in this) {
        sum += selector(element)
    }
    return sum
}