package ai.maum.hitchmed.selfcheck_api_jaju.controller.extension

interface ExtendedController {
    fun handler(description: HandlerProcess.() -> Unit): ResponseType {
        val handlerProcess = HandlerProcess()
        description(handlerProcess)
        handlerProcess.handle()

        return handlerProcess.response!!
    }
}