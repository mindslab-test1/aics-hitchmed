package ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class Request : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "REQUEST_SEQ_GEN",
            sequenceName = "REQUEST_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REQUEST_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var uniqueId: String? = null

    @Column(nullable = false)
    var remoteHost: String? = null

    @Column(nullable = false)
    var requestMethod: String? = null

    @Column(nullable = false)
    var requestPayloadSize: Long? = null

    @Column(nullable = false)
    var requestTime: Instant? = null

    @Column(nullable = false)
    var requestContentType: String? = null

    @Column(nullable = false)
    var pathAbsolute: String? = null

    @Column(nullable = false)
    var responseStatusCode: Long? = null

    @Column(nullable = false)
    var responseTime: Instant? = null
}