package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics.*
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.Typification
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.AgeGroup
import ai.maum.hitchmed.selfcheck_api_jaju.model.statistics.Gender
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class StatisticsUploader(
        var typificationStatisticsRepository: TypificationStatisticsRepository,
        var typificationHashtagStatisticsRepository: TypificationHashtagStatisticsRepository,
        var hashtagStatisticsRepository: HashtagStatisticsRepository,
        var answerStatisticsRepository: AnswerStatisticsRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun proc(gender: Gender, ageGroup: AgeGroup, typification: Typification, solutionCareHashtags: List<String>, tipHashtags: List<String>, choiceRecords: List<AnswerRecord>) {
        // RDB에 문진 기록(선택한 답변, 도출된 결과들 전부)
        val after = Instant.parse(Instant.now().toString().subSequence(0, 10).toString() + "T00:00:00.000Z").minusMillis(1)
        val before = after.plusMillis(1000 * 60 * 60 * 24 + 1)

        val insertDate = after.plusSeconds(60 * 60 * 12)

        logger.info("#S1 " + Instant.now().toString())
        // 10대, 남자, 유형1 -> 단일
        var typistats = typificationStatisticsRepository.findByDateBeforeAndDateAfterAndAgeGroupAndGenderAndTypification(
                before = before,
                after = after,
                ageGroup = ageGroup.toString(),
                gender = gender.toString(),
                typification = typification.number
        )
        if (typistats == null) {
            typistats = TypificationStatistics()
            typistats.ageGroup = ageGroup.toString()
            typistats.count = 0
            typistats.date = insertDate
            typistats.gender = gender.toString()
            typistats.typification = typification.number
        }
        typistats.count = typistats.count!! + 1
        typificationStatisticsRepository.save(typistats)

        logger.info("#S2 " + Instant.now().toString())
        // 유형1, 해시태그1 -> O(해시태그 수)
        for (hashtag in solutionCareHashtags + tipHashtags) {
            var typihashstats = typificationHashtagStatisticsRepository.findByDateBeforeAndDateAfterAndHashtagAndTypification(
                    before = before,
                    after = after,
                    hashtag = hashtag,
                    typification = typification.number
            )
            if (typihashstats == null) {
                typihashstats = TypificationHashtagStatistics()
                typihashstats.count = 0
                typihashstats.date = insertDate
                typihashstats.hashtag = hashtag
                typihashstats.typification = typification.number
            }
            typihashstats.count = typihashstats.count!! + 1
            typificationHashtagStatisticsRepository.save(typihashstats)
        }

        logger.info("#S3 " + Instant.now().toString())
        // 10대, 남자, 해시태그1 -> O(gotlxorm tn)
        for (hashtag in solutionCareHashtags + tipHashtags) {
            var hashstats = hashtagStatisticsRepository.findByDateBeforeAndDateAfterAndAgeGroupAndGenderAndHashtag(
                    before = before,
                    after = after,
                    ageGroup = ageGroup.toString(),
                    gender = gender.toString(),
                    hashtag = hashtag
            )
            if (hashstats == null) {
                hashstats = HashtagStatistics()
                hashstats.ageGroup = ageGroup.toString()
                hashstats.count = 0
                hashstats.date = insertDate
                hashstats.gender = gender.toString()
                hashstats.hashtag = hashtag
            }
            hashstats.count = hashstats.count!! + 1
            hashtagStatisticsRepository.save(hashstats)
        }

        logger.info("#S4 " + Instant.now().toString())
        // 그냥 답변마다 (답변 수)
        for (answer in choiceRecords) {
            var answstats = answerStatisticsRepository.findByDateBeforeAndDateAfterAndAnswer(
                    before = before,
                    after = after,
                    answer = answer.answer
            )
            if (answstats == null) {
                answstats = AnswerStatistics()
                answstats.answer = answer.answer
                answstats.count = 0
                answstats.date = insertDate
            }
            answstats.count = answstats.count!! + 1
            answerStatisticsRepository.save(answstats)
        }
    }
}