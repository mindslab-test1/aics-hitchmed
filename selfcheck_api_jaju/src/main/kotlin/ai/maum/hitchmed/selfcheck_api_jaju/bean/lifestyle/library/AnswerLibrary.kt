package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library

import ai.maum.hitchmed.selfcheck_api_jaju.model.factory.AnswerRecordFactory
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.AnswerRecordRaw
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component
import java.io.File
import javax.annotation.PostConstruct

@DependsOn("recommendationHashtagLibrary")
@Component
class AnswerLibrary {
    var library: List<AnswerRecord> = listOf()
    var libShortAnswers: List<AnswerRecord> = listOf()
    var libChoiceAnswers: List<AnswerRecord> = listOf()

    @PostConstruct
    fun postConstruct() {
        val data = Json { isLenient = true }.decodeFromString<List<AnswerRecordRaw>>(File("./hitchmed_answers.json").readText())

        val KNOWN_SHORT = listOf(101L, 301L)
        library = data.map { AnswerRecordFactory.of(it) }
        libShortAnswers = library.filter { it.answer in KNOWN_SHORT }
        libChoiceAnswers = library.filter { it.answer !in KNOWN_SHORT }
    }
}