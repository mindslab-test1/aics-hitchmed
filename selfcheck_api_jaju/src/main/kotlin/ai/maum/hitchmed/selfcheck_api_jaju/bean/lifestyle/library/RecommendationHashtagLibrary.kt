package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.RecommendationHashtagLibraryException
import ai.maum.hitchmed.selfcheck_api_jaju.model.factory.RecommendationHashtagRecordFactory
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.RecommendationHashtagRecordRaw
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Component
import java.io.File
import javax.annotation.PostConstruct

@Component("recommendationHashtagLibrary")
class RecommendationHashtagLibrary {
    var library: List<RecommendationHashtagRecord> = listOf()

    @PostConstruct
    fun postConstruct() {
        val data = Json { isLenient = true }.decodeFromString<List<RecommendationHashtagRecordRaw>>(File("./hitchmed_recommendation_hashtags.json").readText())

        library = data.map { RecommendationHashtagRecordFactory.of(it) }
    }

    fun of(numbering: String): RecommendationHashtagRecord = library.find { it.hashtag_numbering == numbering }
            ?: throw RecommendationHashtagLibraryException(1, "Invalid numbering")
}