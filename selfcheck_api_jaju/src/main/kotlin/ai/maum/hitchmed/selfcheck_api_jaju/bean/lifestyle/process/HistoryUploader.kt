package ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process

import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.AnswerItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.ProductItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.TipItemDto
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.lifestyle.*
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.Typification
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class HistoryUploader(
        var answerRepository: AnswerRepository,
        var productRepository: ProductRepository,
        var resultRepository: ResultRepository,
        var tipRepository: TipRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun proc(uniqueId: String, answers: List<AnswerItemDto>, typification: Typification, solutionCareHashtags: List<String>, productRecommendations: List<ProductItemDto>, tips: List<TipItemDto>) {
        logger.info("#H1 " + Instant.now().toString())

        // Save user answers
        for (answer in answers) {
            val answerEntity = Answer()
            answerEntity.uniqueId = uniqueId
            answerEntity.question = answer.question
            answerEntity.answerShort = if (answer.is_short) answer.answer_short else null
            answerEntity.answerChoice = if (!answer.is_short) answer.answer_choice.toString() else null

            answerRepository.save(answerEntity)
        }

        logger.info("#H2 " + Instant.now().toString())

        // save recommended products
        for (product in productRecommendations) {
            val productEntity = Product()
            productEntity.uniqueId = uniqueId
            productEntity.code = product.code

            productRepository.save(productEntity)
        }

        logger.info("#H3 " + Instant.now().toString())

        // save hashtag & typification results
        val resultEntity = Result()
        resultEntity.uniqueId = uniqueId
        resultEntity.recommendation = solutionCareHashtags.toString()
        resultEntity.typification = typification.number

        resultRepository.save(resultEntity)

        logger.info("#H4 " + Instant.now().toString())

        // save tip results
        for (tip in tips) {
            val tipEntity = Tip()

            tipEntity.uniqueId = uniqueId
            tipEntity.hashtag = tip.hashtag
            tipEntity.isLifeStyle = tip.is_lifestyle_tip

            tipRepository.save(tipEntity)
        }

        logger.info("#H5 " + Instant.now().toString())
    }
}