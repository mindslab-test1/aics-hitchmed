package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import ai.maum.hitchmed.selfcheck_api_jaju.jpa.AllOpen
import ai.maum.hitchmed.selfcheck_api_jaju.jpa.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class HashtagStatistics : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "HASHTAG_STATS_SEQ_GEN",
            sequenceName = "HASHTAG_STATS_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HASHTAG_STATS_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var gender: String? = null

    @Column(nullable = false)
    var ageGroup: String? = null

    @Column(nullable = false)
    var hashtag: String? = null

    @Column(nullable = false)
    var count: Long? = null

    @Column(nullable = false, name = "\"SDATE\"")
    var date: Instant? = null
}