package ai.maum.hitchmed.selfcheck_api_jaju.model.factory

import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.TipHashtagRecord
import ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset.TipHashtagRecordRaw

class TipHashtagRecordFactory {
    companion object {
        fun of(from: TipHashtagRecordRaw): TipHashtagRecord =
                TipHashtagRecord(hashtag_numbering = from.numbering, hashtag_name = from.hashtag, category = from.category, content = from.content)
    }
}