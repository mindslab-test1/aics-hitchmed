package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import kotlinx.serialization.Serializable

@Serializable
data class RecommendationHashtagRecord(
        var hashtag_numbering: String = "",
        var hashtag_name: String = "",
        var category: Category = Category.NOT_USED
) {
    enum class Category {
        HEALTH,
        LIFESTYLE,
        MENTAL,
        NOT_USED
    }
}