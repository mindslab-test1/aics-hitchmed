package ai.maum.hitchmed.selfcheck_api_jaju.http.dto.statistics

import kotlinx.serialization.Serializable

@Serializable
class StatisticsHashtagResponseItemDto(
        var tag: String,
        var gender: String,
        var age0_24: Long,
        var age25_34: Long,
        var age35_44: Long,
        var age45_54: Long,
        var age55_64: Long,
        var age65_99: Long
)