package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle

import ai.maum.hitchmed.selfcheck_api_jaju.model.exception.TypificationException

class Typification private constructor(val number: String, val name: String, val healthGroup: HealthGroup, val principal: Principal) {
    companion object {
        val g001 = Typification("G001", "자존감 쩌는 필라테스 강사", HealthGroup.FITNESS_HEALTH, Principal.Se)
        val g002 = Typification("G002", "차근차근 다이어터", HealthGroup.FITNESS_HEALTH, Principal.Si)
        val g003 = Typification("G003", "자신만의 체중관리비법가", HealthGroup.FITNESS_HEALTH, Principal.Ne)
        val g004 = Typification("G004", "효율중시 피트니스", HealthGroup.FITNESS_HEALTH, Principal.Ni)
        val g005 = Typification("G005", "도전적인 헬스장사장님", HealthGroup.FITNESS_HEALTH, Principal.Te)
        val g006 = Typification("G006", "자존감 쩌는 홈트레이닝마스터", HealthGroup.FITNESS_HEALTH, Principal.Ti)
        val g007 = Typification("G007", "센스있는 필라테스선생님", HealthGroup.FITNESS_HEALTH, Principal.Fe)
        val g008 = Typification("G008", "집순이 다이어터", HealthGroup.FITNESS_HEALTH, Principal.Fi)

        val g009 = Typification("G009", "야근을 일삼는 인스타 모델", HealthGroup.FATIGUE_SLEEP, Principal.Se)
        val g010 = Typification("G010", "피로누적 신입사원", HealthGroup.FATIGUE_SLEEP, Principal.Si)
        val g011 = Typification("G011", "아이디어뱅크라서 피로누적", HealthGroup.FATIGUE_SLEEP, Principal.Ne)
        val g012 = Typification("G012", "잡생각하다보니 수면부족", HealthGroup.FATIGUE_SLEEP, Principal.Ni)
        val g013 = Typification("G013", "총대메다 피로누적", HealthGroup.FATIGUE_SLEEP, Principal.Te)
        val g014 = Typification("G014", "남못믿다 피로누적", HealthGroup.FATIGUE_SLEEP, Principal.Ti)
        val g015 = Typification("G015", "리액션하다가 피로누적", HealthGroup.FATIGUE_SLEEP, Principal.Fe)
        val g016 = Typification("G016", "피로누적 집순이", HealthGroup.FATIGUE_SLEEP, Principal.Fi)

        val g017 = Typification("G017", "만사귀찮 스트레스", HealthGroup.STRESS_IMMUNITY, Principal.Se)
        val g018 = Typification("G018", "힘든계획에 혓바늘섰네", HealthGroup.STRESS_IMMUNITY, Principal.Si)
        val g019 = Typification("G019", "머리굴리다 스트레스", HealthGroup.STRESS_IMMUNITY, Principal.Ne)
        val g020 = Typification("G020", "나혼자 생각할시간이 필요하다", HealthGroup.STRESS_IMMUNITY, Principal.Ni)
        val g021 = Typification("G021", "총대메다 입술부르텄네", HealthGroup.STRESS_IMMUNITY, Principal.Te)
        val g022 = Typification("G022", "일하다가 병걸리겠네", HealthGroup.STRESS_IMMUNITY, Principal.Ti)
        val g023 = Typification("G023", "남챙기다 내 면역력떨어졌네", HealthGroup.STRESS_IMMUNITY, Principal.Fe)
        val g024 = Typification("G024", "면역력필요한 집순이", HealthGroup.STRESS_IMMUNITY, Principal.Fi)

        fun of(number: String) =
                when (number) {
                    "G001" -> g001
                    "G002" -> g002
                    "G003" -> g003
                    "G004" -> g004
                    "G005" -> g005
                    "G006" -> g006
                    "G007" -> g007
                    "G008" -> g008
                    "G009" -> g009
                    "G010" -> g010
                    "G011" -> g011
                    "G012" -> g012
                    "G013" -> g013
                    "G014" -> g014
                    "G015" -> g015
                    "G016" -> g016
                    "G017" -> g017
                    "G018" -> g018
                    "G019" -> g019
                    "G020" -> g020
                    "G021" -> g021
                    "G022" -> g022
                    "G023" -> g023
                    "G024" -> g024
                    else -> throw TypificationException(1, "Invalid typification number")
                }

        fun of(group: HealthGroup, principal: Principal) = when (group) {
            HealthGroup.FITNESS_HEALTH -> when (principal) {
                Principal.Se -> g001
                Principal.Si -> g002
                Principal.Ne -> g003
                Principal.Ni -> g004
                Principal.Te -> g005
                Principal.Ti -> g006
                Principal.Fe -> g007
                Principal.Fi -> g008
            }
            HealthGroup.FATIGUE_SLEEP -> when (principal) {
                Principal.Se -> g009
                Principal.Si -> g010
                Principal.Ne -> g011
                Principal.Ni -> g012
                Principal.Te -> g013
                Principal.Ti -> g014
                Principal.Fe -> g015
                Principal.Fi -> g016
            }
            HealthGroup.STRESS_IMMUNITY -> when (principal) {
                Principal.Se -> g017
                Principal.Si -> g018
                Principal.Ne -> g019
                Principal.Ni -> g020
                Principal.Te -> g021
                Principal.Ti -> g022
                Principal.Fe -> g023
                Principal.Fi -> g024
            }
        }
    }

    override fun toString(): String = number
}

enum class HealthGroup {
    FITNESS_HEALTH,
    FATIGUE_SLEEP,
    STRESS_IMMUNITY;

    companion object {
        fun of(fitness_health: Long, fatigue_sleep: Long, stress_immunity: Long) =
                if (stress_immunity > fitness_health && stress_immunity > fatigue_sleep) STRESS_IMMUNITY
                else if (fatigue_sleep > fitness_health && fatigue_sleep > stress_immunity) FATIGUE_SLEEP
                else FITNESS_HEALTH
    }
}

enum class Principal {
    Se,
    Si,
    Ne,
    Ni,
    Te,
    Ti,
    Fe,
    Fi;

    companion object {
        fun of(e: Boolean, s: Boolean, t: Boolean, j: Boolean): Principal =
                if (e) {
                    if (j) {
                        if (t) Te else Fe
                    } else {
                        if (s) Se else Ne
                    }
                } else { // i
                    if (j) {
                        if (s) Si else Ni
                    } else {
                        if (t) Ti else Fi
                    }
                }
    }
}