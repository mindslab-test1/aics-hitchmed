package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import kotlinx.serialization.Serializable

@Serializable
data class RecommendationHashtagRecordRaw(
        var number: String = "",
        var hashtag: String = "",
        var category: String = ""
)