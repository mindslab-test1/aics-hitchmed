package ai.maum.hitchmed.selfcheck_api_jaju.http.rdo

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import java.io.Serializable

// Request Data Object
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
data class UserInfo(
        var tbd: Long? = null
) : Serializable