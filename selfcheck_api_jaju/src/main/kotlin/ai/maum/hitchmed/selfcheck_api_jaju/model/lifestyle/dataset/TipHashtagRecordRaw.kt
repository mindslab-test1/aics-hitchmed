package ai.maum.hitchmed.selfcheck_api_jaju.model.lifestyle.dataset

import kotlinx.serialization.Serializable

@Serializable
data class TipHashtagRecordRaw(
        var numbering: String = "",
        var hashtag: String = "",
        var category: String = "",
        var content: String = ""
)