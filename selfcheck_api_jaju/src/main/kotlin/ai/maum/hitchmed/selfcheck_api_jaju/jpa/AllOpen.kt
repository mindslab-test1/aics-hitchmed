package ai.maum.hitchmed.selfcheck_api_jaju.jpa

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class AllOpen