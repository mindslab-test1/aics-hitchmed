package ai.maum.hitchmed.selfcheck_api_jaju.jpa.statistics

import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface TypificationStatisticsRepository : CrudRepository<TypificationStatistics, Long> {
    fun findByDateBeforeAndDateAfter(before: Instant, after: Instant): List<TypificationStatistics>
    fun findByDateBeforeAndDateAfterAndAgeGroupAndGenderAndTypification(before: Instant, after: Instant, ageGroup: String, gender: String, typification: String): TypificationStatistics?
}