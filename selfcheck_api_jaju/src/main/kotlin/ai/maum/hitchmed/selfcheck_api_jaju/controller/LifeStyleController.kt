package ai.maum.hitchmed.selfcheck_api_jaju.controller

import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.library.AnswerLibrary
import ai.maum.hitchmed.selfcheck_api_jaju.bean.lifestyle.process.*
import ai.maum.hitchmed.selfcheck_api_jaju.controller.extension.ExtendedController
import ai.maum.hitchmed.selfcheck_api_jaju.controller.impl.lifestyle.LifeStyleGet
import ai.maum.hitchmed.selfcheck_api_jaju.http.dto.lifestyle.LifeStyleGetRequestDto
import ai.maum.hitchmed.selfcheck_api_jaju.http.rdo.RequestInfo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class LifeStyleController(
        var answerLibrary: AnswerLibrary,
        var personalInformationParser: PersonalInformationParser,
        var typifier: Typifier,
        var hashtagCalculator: HashtagCalculator,
        var productRecommender: ProductRecommender,
        var tipGenerator: TipGenerator,
        var historyUploader: HistoryUploader,
        var statisticsUploader: StatisticsUploader,
        var requestInfoFactory: ObjectFactory<RequestInfo>
) : ExtendedController {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Transactional
    @PostMapping("/api/lifestyle/get")
    fun get(@RequestBody requestDto: LifeStyleGetRequestDto) = handler {
        w controller this@LifeStyleController
        w dto requestDto
        w handler LifeStyleGet(this@LifeStyleController, requestDto)
    }
}